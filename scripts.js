$(document).ready(function() {
    $(document).on('submit', '#my-form', function(e){
        e.preventDefault();
        // убираем класс ошибок с инпутов
        $('input').each(function(){
            $(this).removeClass('error_input');
        });
        // прячем текст ошибок
        $('.error').hide();
        $('.error-message').hide();

        // получение данных из полей
        var name = $('#name').val();
        var email = $('#email').val();
        var phone = $('#phone').val();

        $.ajax({
            // метод отправки
            type: "POST",
            // путь до скрипта-обработчика
            url: "/request.php",
            // какие данные будут переданы
            data: {
                'name': name,
                'email': email,
                'phone': phone
            },
            // тип передачи данных
            dataType: "json",
            // действие, при ответе с сервера
            success: function(data) {
                if (data.success) {
                    alert('Форма корректно заполнена!');
                } else {
                    for (var error_item in data.validate) {
                        $('input[name="' + error_item + '"] + .error-message').text(data.validate[error_item]);
                        document.querySelector('input[name="' + error_item + '"] + .error-message').style.display = 'inline';
                        }
                    }
                }
            });
        // останавливаем сабмит, чтоб не перезагружалась страница
        return false;
    });
});
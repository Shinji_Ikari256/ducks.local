<?php

function validate_name($value) {
    return $value != '' ? 0 : 1;
}

function validate_email($value) {
    return $value != '' ? filter_var($value, FILTER_VALIDATE_EMAIL) ? 0 : 2 : 1;
}

function validate_phone($value) {
    return $value != '' ? strlen($value) == 12 ? 0 : 2 : 1;
}

$response = [
    'success' => true
];

foreach($_POST as $fieldName => $oneField) {
    if (function_exists('validate_'.$fieldName)) {

        $error_code = call_user_func('validate_'.$fieldName, $oneField);

        if ($error_code) {
            $response['success'] = false;
            $response['validate'][$fieldName] = 'Поле ' . ($error_code == 1 ? 'обязательно для заполнения!' : 'должно быть введено корректно!' );
        }
    }
}

echo json_encode($response);
die(0);
<!DOCTYPE html>
<head>
	<title>Форма</title>
	<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<form action="request.php" id="my-form" method="POST">

			<div>
				<label class="label_desc">Имя:</label>
				<input type="text" name='name' id="name"/>
				<label type="hidden" class="error-message" for="name"></label>
			</div>

			<div>
				<label class="label_desc">Почта:</label>
				<input type="text" name='email' id="email"/>
				<label type="hidden" class="error-message" for="email"></label>
			</div>

			<div>
				<label class="label_desc">Телефон:</label>
				<input type="text" name='phone' id="phone"/>
				<label type="hidden"  class="error-message none" for="phone"></label>
			</div>

			<div>
				<button>Отправить</button>
			</div>
		
		</form>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"
				integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
				crossorigin="anonymous"></script>
		<script src="scripts.js"></script>
</body>
</html>
